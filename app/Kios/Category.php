<?php  namespace Kios;

use Baum\Node;

class Category extends Node {
    public function products()
    {
        return $this->hasMany('Product');
    }
}