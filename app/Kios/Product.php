<?php  namespace Kios; 

use Illuminate\Database\Eloquent\Model;

class Product extends Model {
    public function category()
    {
        return $this->belongsTo('Category');
    }

    public function vendor()
    {
        return $this->belongsTo('Vendor');
    }

    public function supplier()
    {
        return $this->belongsTo('Supplier');
    }
    
    public function tags()
    {
        return $this->hasMany('Tag');
    }
}
