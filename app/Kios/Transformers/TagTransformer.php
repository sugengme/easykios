<?php  namespace Kios\Transformers;

use Kios\Tag;
use League\Fractal\TransformerAbstract;

class TagTransformer extends TransformerAbstract {
    public function transform(Tag $tag)
    {
        return [
            'id' => (int) $tag->id,
            'name' => $tag->name,
            'slug' => $tag->slug
        ];
    }
}