<?php
use Faker\Factory as Faker;
use EasyKios\Products\Product;

class ProductsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 100) as $index)
		{
			Product::create([
                'name' => $faker->name,
                'description' => $faker->paragraph(),
                'category_id' => $faker->randomDigit,
                'vendor_id' => $faker->randomDigit,
                'supplier_id' => $faker->randomDigit,
                'weight' => '1',
                'base_price' => $faker->randomNumber(),
                'stock' => $faker->randomDigit,
                'status' => 1
			]);
		}
	}

}