<?php

use Faker\Factory as Faker;
use Kios\Category;

class CategoriesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 5) as $index)
		{
			Category::create([
				'name' => $faker->word,
				'slug' => $faker->word,
				'description' => $faker->realText()
			]);
		}

		$roots = Category::whereNull('parent_id')->get();

		foreach ($roots as $root)
		{
			foreach (range(1, 7) as $index)
			{
				$children = Category::create([
					'name' => $faker->word,
					'slug' => $faker->word,
					'description' => $faker->paragraph()
				]);

				$children->makeChildOf($root);
			}

		}
	}

}