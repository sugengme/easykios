<?php

use Faker\Factory as Faker;
use Kios\Color;

class ColorsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Color::create([
				'name' => $faker->colorName,
				'description' => $faker->text(),
				'hex'	=> $faker->hexcolor,
				'status' => 1
			]);
		}
	}

}