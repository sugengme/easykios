<?php

use Kios\Gender;

class GendersTableSeeder extends Seeder {

	protected $genderName = [
		'Pria',
		'Wanita'
	];

	public function run()
	{

		foreach ($this->genderName as $gender)
		{
			Gender::create([
				'name' => $gender
			]);
		}
	}

}