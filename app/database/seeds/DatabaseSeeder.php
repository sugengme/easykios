<?php

class DatabaseSeeder extends Seeder {

	protected $tables = [
		'genders',
		'colors',
		'tags',
		'categories'
	];

	protected $seeders = [
		'GendersTableSeeder',
		'ColorsTableSeeder',
		'TagsTableSeeder',
		'CategoriesTableSeeder'
	];

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->cleanDatabase();

		foreach ($this->seeders as $seeder) {
		    $this->call($seeder);
		}
	}

	/**
	 * Clean database from previous values
	 *
	 * @return void
     */
	protected function cleanDatabase()
	{
		foreach ($this->tables as $table)
		{
			DB::table($table)->truncate();
		}
	}

}
