<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuppliersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('suppliers', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name', 50);
            $table->string('description');
            $table->string('logo', 100);
            $table->string('phone', 20);
            $table->string('mobile', 20);
            $table->string('fax', 20);
            $table->string('address1');
            $table->string('address2');
            $table->integer('city');
            $table->integer('state');
            $table->string('zipcode', 15);
            $table->integer('country');
            $table->string('email', '50');
            $table->string('website', 50);
            $table->integer('payment_method');
            $table->integer('user_entry');
            $table->integer('user_update');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('suppliers');
	}

}
