<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShippersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shippers', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name', 50);
            $table->string('phone', 20);
            $table->string('logo', 50);
            $table->enum('status', ['1', '0'])->default('1');
            $table->integer('user_entry');
            $table->integer('user_update');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('shippers');
	}

}
