<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCostumersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('costumers', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('group_id');
            $table->integer('salutation');
            $table->string('firstname', 100);
            $table->string('lastname', 100);
            $table->integer('gender');
            $table->string('password', 100);
            $table->string('email', 100);
            $table->date('birthday');
            $table->enum('newsletter', ['1', '0'])->default('0');
            $table->dateTime('newsletter_date_add');
            $table->dateTime('registration_date');
            $table->dateTime('last_visit');
            $table->text('note');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('costumers');
	}

}
