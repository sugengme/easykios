<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCostumerAddressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('costumer_address', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('costumer_id');
            $table->string('alias', 100);
            $table->string('company_name');
            $table->string('address1');
            $table->string('address2');
            $table->integer('city');
            $table->integer('state');
            $table->string('zipcode', 15);
            $table->integer('country');
            $table->string('phone', 20);
            $table->string('mobile', 20);
            $table->text('note');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('costumer_address');
	}

}
