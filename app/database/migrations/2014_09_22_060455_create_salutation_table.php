<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSalutationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('salutation', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name', 20);
            $table->integer('gender_id');
            $table->enum('status', ['1', '0'])->default('1');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('salutation');
	}

}
