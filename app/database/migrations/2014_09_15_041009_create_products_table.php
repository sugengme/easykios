<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->integer('category_id');
            $table->integer('vendor_id');
            $table->integer('supplier_id');
            $table->integer('weight');
            $table->decimal('base_price');
            $table->integer('stock');
            $table->enum('status', ['1', '0'])->default('1');
            $table->integer('user_entry');
            $table->integer('user_update');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
