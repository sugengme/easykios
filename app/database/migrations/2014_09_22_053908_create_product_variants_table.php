<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductVariantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_variants', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name', 100);
            $table->string('slug', 150);
            $table->string('sku', 50);
            $table->text('description');
            $table->string('short_description');
            $table->decimal('price', 17);
            $table->decimal('special_price', 17);
            $table->integer('weight');
            $table->integer('stock');
            $table->integer('size');
            $table->integer('color');
            $table->integer('material');
            $table->integer('pattern');
            $table->integer('discount');
            $table->integer('rank');
            $table->enum('status', ['1', '0'])->default('1');
            $table->integer('user_entry');
            $table->integer('user_update');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_variants');
	}

}
