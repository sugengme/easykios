<?php

Route::get('/', function()
{
	return View::make('hello');
});

Route::group(['namespace' => 'API'], function()
{
	Route::resource('tags', 'TagsController');
});


