<?php namespace API;

use Kios\Tag;
use Kios\Transformers\TagTransformer;
use Sorskod\Larasponse\Larasponse;

class TagsController extends APIController {

	protected $fractal;

	public function __construct(Larasponse $fractal)
	{
		$this->fractal = $fractal;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tags = Tag::all();

		return $this->fractal->collection($tags, new TagTransformer());
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$tag = Tag::find($id);

		return $this->fractal->item($tag, new TagTransformer());
	}


}
